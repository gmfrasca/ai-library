# Ansible

This library contains ansible roles to cleanup and deploy models in AI Library as Seldon applications. The scripts create REST endpoints for the models in AI Library through seldon model serving.

# Ansible Scripts

## roles/deploy_actions

`deploy.yaml` - File to activate role 'deploy_models'

## roles/deploy_models/tasks

`deploy_models.yml` - deploy the above models.

## roles/deploy_actions/defaults

`main.yml` - parameter definition for commands in deploy_models.yml

# Command

## Steps

The following steps deploy models in AI Library through Seldon.

1. cd ansible
2. ansible-playbook roles/deploy_actions/deploy.yml

After the deployment completes, the TOKEN value for each model is retrieved automatically and stored under <model>.token in the workspace folder (defined in main.yml). The following command can also be used to retrieve tokens if the initial token expires.

    curl -XPOST -u <auth-key>:<auth-secret> <seldon-apiserver-endpoint>/oauth/token -d 'grant_type=client_credentials'


