# Linear Regression

Linear regression is used to establish the relationship between a dependent variable and one (simple linear regression) or more independent variables (multivariate regression) using a best fit straight line.

## Contents

`regression.py` - Linear regression model.
##### Parameters
* s3endpointUrl - Endpoint to s3 storage backend
* s3accessKey - Access key to s3 storage backend
* s3secretKey - Secret key to s3 storage backend
* s3objectStoreLocation - Bucket name in s3 backend
* s3Path - object key containing test failures to run prediction on (location in the bucket)
* s3Destination - location to store the prediction results
* training - training data set (csv of independent variables with last columen being the dependent variable)
* prediction - prediction data set

## Workflow

### Save Data

#### Prediction Data

The following example shows sample s3Path that points to the folder where prediction data is stored.

    s3Path - risk_analysis
    Files -
        training.csv
        prediction.csv

The following example shows what an individual training data looks like. 

    training.csv
       S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,Risk
       0,0,1,0,0,0,0,0,0,0,0.8
       0,0,1,0,0,0,1,1,0,0,0.5
       ..
       ..


### Run Model

#### Prediction

    curl -v --header "Authorization: Bearer $TOKEN \
    http://seldon-core-apiserver-panbalag.apps.cluster-dc86.dc86.openshiftworkshop.com/api/v0.1/predictions -d \
    '{"strData":"s3endpointUrl='"$S3ENDPOINTURL"', s3accessKey='"$S3ACCESSKEY"', \
      s3secretKey='"$S3SECRETKEY"', s3objectStoreLocation=DH-DEV-DATA, \
      s3Path=risk_analysis/, s3Destination=risk_analysis/results, \
      training=training.csv, prediction=prediction.csv"}' \
      -H "Content-Type: application/json"

### Use Results

#### Prediction

The following example shows sample s3Destination that points to the folder where prediction results are stored.

    s3Destination - risk_analysis/results
    Files -
        results.txt

    results.txt contains json data with predicted values and accuracy measures Mean Absolute Error (MAE) and Median Absolute Error (MdAE).

