# Flake Analysis

Flake analysis is used to identify false positive in test failures. The model uses DBSCAN clustering algorithm to cluster false positives and then predict the chances of a given failure being a false positive.

## Contents

`flakes_training.py` - Clustering (DBSCAN) based model to group false positives in test failures
##### Parameters
* s3endpointUrl - Endpoint to s3 storage backend
* s3accessKey - Access key to s3 storage backend
* s3secretKey - Secret key to s3 storage backend
* s3objectStoreLocation - Bucket name in s3 backend
* s3Path - object key containing training data (location in the bucket)
* s3Destination - location to store the trained model (object key including model name)

## Workflow

### Save Data

#### Training Data

The following example shows sample s3Path that points to the folder where training data is stored.

    s3Path - flake_analysis/datasets/training/records
    Files -
        record1.json
        record2.json
        record3.json
        ..

The following example shows what an individual training data looks like. 

    record1.json
    {
        "id":"041f6832-aa14-4f6e-891d-31aaf8d7ed01",
        "status":"failure",
        "pull":7331,
        "log":"# ----------------------------------------------------------------------\n# testFormatTypes (check_storage_format.TestStorage)\n#\n> Using older 'udisks2' implementation: 2.1.8\nnot ok 26 testFormatTypes (check_storage_format.TestStorage) duration: 78s\nTraceback (most recent call last):\n  File \"/build/cockpit/bots/../test/verify/check-storage-format\", line 74, in testFormatTypes\n    check_type(\"ext4\")\n  File \"/build/cockpit/bots/../test/verify/check-storage-format\", line 71, in check_type\n    self.content_row_wait_in_col(1, 1, type + \" File System\")\n  File \"/build/cockpit/test/verify/storagelib.py\", line 89, in content_row_wait_in_col\n    self.retry(None, lambda: self.browser.is_present(col) and val in self.browser.text(col), None)\n  File \"/build/cockpit/test/verify/storagelib.py\", line 65, in retry\n    b.wait_checkpoint()\n  File \"/build/cockpit/test/common/testlib.py\", line 291, in wait_checkpoint\n    return self.phantom.wait_checkpoint()\n  File \"/build/cockpit/test/common/testlib.py\", line 821, in <lambda>\n    return lambda *args: self._invoke(name, *args)\n  File \"/build/cockpit/test/common/testlib.py\", line 847, in _invoke\n    raise Error(res['error'])\nError: timeout happened\nWrote TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.png\nWrote TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.html\nWrote TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.js.log\nJournal extracted to TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.log\n",
        "test":"testFormatTypes (check_storage_format.TestStorage)",
        "context":"verify/debian-testing",
        "date":"2017-07-19T11:56:01Z",
        "merged":true,
        "revision":"b32635869b9e87cdd9e42b6e6123150d500f6862"
    }

### Run Model

#### Training

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/flake-analysis-training?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"               : "flake-analysis-training", 
       "app_args": "-s3Path=flake_analysis/datasets/training/records -s3Destination=flake_analysis/models/testflakes.model"}'

### Use Results

#### Training

The following example shows sample s3Destination that points to where the trained model is stored.

    s3Destination - flake_analysis/models/testflakes.model
    File -
        testflakes.model
