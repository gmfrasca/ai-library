import datetime
import time
import csv
import io
import os
import uuid
import argparse
import subprocess
import zipfile
import json
import re
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3

class flakes_prediction(object):

    def __init__(self):
        self.model = 'None'

    def predict(self,data,features_names):
        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["s3Path", "s3endpointUrl", "s3objectStoreLocation", "model",
                   "s3accessKey", "s3secretKey", "s3Destination"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result
        model = params['model']
        s3Path = params['s3Path']
        s3endpointUrl = params['s3endpointUrl']
        s3objectStoreLocation = params['s3objectStoreLocation']
        s3accessKey = params['s3accessKey']
        s3secretKey = params['s3secretKey']
        s3Destination = params['s3Destination']

        MODEL_PATH = '/tmp/bots/images/'
        if not os.path.exists(MODEL_PATH):
            os.makedirs(MODEL_PATH)

        modelurl = model.split("/")
        modelfile = modelurl[-1]

        # Download the trained model from storage backend in to MODEL_PATH
        session = s3.create_session_and_resource(s3accessKey,
                                                 s3secretKey,
                                                 s3endpointUrl)
        s3.download_file(session,
                         s3objectStoreLocation,
                         model,
                         MODEL_PATH + modelfile)

        if not os.path.exists('/tmp/results'):
            os.makedirs('/tmp/results')

        objects = s3.get_objects(session, s3objectStoreLocation, s3Path)

        for key in objects:
            obj = session.Object(s3objectStoreLocation, key)
            contents = obj.get()['Body'].read().decode('utf-8')
            keylist = key.split("/")
            filename = keylist[-1]
            if contents:
                jcontents = json.loads(contents)
                log = jcontents['log']
                label = jcontents['label']
                with open('/tmp/tmp.txt', 'w') as outfile:
                    outfile.write(log)
                cat_file = 'cat /tmp/tmp.txt '
                pipe_predict = '| python bots/tests-policy -model '
                get_flakes = '| grep Flake > /tmp/result.txt ; cat /tmp/result.txt'
                cmd = cat_file + pipe_predict + modelfile + ' tmp.txt ' + get_flakes
                result = os.popen(cmd).read()
                flake_probability = re.findall("\d+\.\d+", result)
                if flake_probability:
                    jcontents['flake'] = flake_probability[0]
                else:
                    jcontents['flake'] = 0.0
                filepath = '/tmp/results/' + filename
                with open(filepath, 'w') as fp:
                    json.dump(jcontents, fp)
                    fp.write('\n')

        print("Prediction complete!")
        # Upload results to storage
        s3.upload_folder(s3accessKey,
                         s3secretKey,
                         s3endpointUrl,
                         s3objectStoreLocation,
                         '/tmp/results',
                         s3Destination)
        return result


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = flakes_prediction()
  obj.predict(data,20)

if __name__== "__main__":
  main()
